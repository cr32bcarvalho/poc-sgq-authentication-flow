import json
import boto3
import os
import hmac
import hashlib
import base64

CLIENT_ID = os.environ['CLIENT_ID']
CLIENT_SECRET = os.environ['CLIENT_SECRET']
ADD_TO_GROUP_FUNCTION = os.environ['ADD_TO_GROUP_FUNCTION']

ERROR = 0
SUCCESS = 1

def lambda_handler(event, context):
    name = event['name']
    phone_number = event['phone_number']
    email = event['email']
    department = event['department']
    group = event['group']
    password = event['password']
    
    
    status, msg = sign_up(name, phone_number, email, password, department, group)
    
    if status == ERROR:
        return msg
    if status == SUCCESS:
        return msg

def sign_up(name, phone_number, email, password, department, group = "contributors"):
    client = boto3.client('cognito-idp')
    lambda_client = boto3.client('lambda')

    try:
        resp = client.sign_up(
            ClientId=CLIENT_ID,
            SecretHash=get_secret_hash(email),
            Username=email,
            Password=password,
            UserAttributes=[
                {
                    'Name': 'name',
                    'Value': name
                },
                {
                    'Name': 'phone_number',
                    'Value': phone_number
                },
                {
                    'Name': 'email',
                    'Value': email
                },
                {
                    'Name': 'custom:department',
                    'Value': department
                },
            ]
        )
    except client.exceptions.UsernameExistsException as e:
        return ERROR, {'status': 'fail', 'msg': "An account with the given email: %s already exists" % email}
    except Exception as e:
        return  ERROR, e.__str__()
        
    try: 
        resp, message = invokeAddUserToGroup(email, group)
        if resp == SUCCESS:
            return SUCCESS, {'status': 'success', 'msg': "Welcome %s" % name, 'group': group}
        return ERROR, message
    except Exception as e:
        return  ERROR, e.__str__()
        
def invokeAddUserToGroup(username, group):
    lambda_client = boto3.client('lambda')

    data = {'username': username, 'group': group}

    response = lambda_client.invoke(FunctionName=ADD_TO_GROUP_FUNCTION,
                             InvocationType='RequestResponse',
                             Payload=json.dumps(data))

    result = json.loads(response.get('Payload').read())
    return result
    
def get_secret_hash(username):
    msg = username + CLIENT_ID
    dig = hmac.new(str(CLIENT_SECRET).encode('utf-8'),
        msg = str(msg).encode('utf-8'),   digestmod=hashlib.sha256).digest()
    d2 = base64.b64encode(dig).decode()
    return d2