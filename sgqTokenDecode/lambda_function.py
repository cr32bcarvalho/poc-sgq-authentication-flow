import json
from sgq_jwt.decode import decode_token

def lambda_handler(event, context):
    # TODO implement
    try: 
        decoded = decode_token(event['token'])
        return decoded
    except Exception as e:
        return  e.__str__()
    