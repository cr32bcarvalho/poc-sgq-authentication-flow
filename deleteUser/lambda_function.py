import json
import boto3
import os

USER_POOL_ID = os.environ['USER_POOL_ID']

def lambda_handler(event, context):
    username = event['username']

    client = boto3.client('cognito-idp')

    try: 
        client.admin_delete_user( UserPoolId=USER_POOL_ID, Username=username )
        return {'status': 'success', 'msg': "User %s has ben deleted" % username}
    except Exception as e:
        return  e.__str__()
    
