import json
import boto3
import os

USER_POOL_ID = os.environ['USER_POOL_ID']

ERROR = 0
SUCCESS = 1

def lambda_handler(event, context):
    username = event['username']
    group = event['group']

    client = boto3.client('cognito-idp')

    try: 
        resp = client.admin_add_user_to_group( UserPoolId=USER_POOL_ID, Username=username, GroupName=group )
        return SUCCESS, {'status': 'success', 'msg': "User %s added to group" % username}
    except Exception as e:
        return  ERROR, e.__str__()
    
